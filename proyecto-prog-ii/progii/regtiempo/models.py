from django.db import models
from django.utils import timezone


class Tiempo(models.Model):
    desde = models.DateTimeField(auto_now_add=True)
    hasta = models.DateTimeField(null=True, blank=True, editable=False)
    tiempo = models.DurationField(default=0, editable=False)
    corriendo = models.BooleanField(default=True, editable=False)
    
    
    class Meta:
        abstract = True


class Tarea(Tiempo):
    subtarea_de = models.ForeignKey(
        'self', on_delete=models.CASCADE, null=True, blank=True)
    titulo = models.CharField(max_length=60, unique=True)
    
    
    def __str__(self):
        return self.titulo
    
    def calcular_tiempo(self):
        pass


class Intervalo(Tiempo):
    intervalo_de = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    
    
    def __str__(self):
        pass
    
    def calcular_tiempo(self):
        if self.corriendo:
            return timezone.now() - self.desde
        else:
            return self.hasta - self.desde
        
    def detener(self):
        if self.corriendo:
            self.hasta = timezone.now()
            self.corriendo = False
            self.tiempo = self.calcular_tiempo()
