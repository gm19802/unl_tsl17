from django.apps import AppConfig


class RegtiempoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'regtiempo'
